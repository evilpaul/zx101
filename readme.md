# ZX 101
An introduction to demo-coding on the ZX Spectrum

## More info

Find more info in my 2018 seminar https://www.youtube.com/watch?v=M6Zi8xLCbMs

## Building
Build tools only work on Windows
You'll need Python3 installed
Pull the repo
Type `make`
Look for the output file `build\demo.tap`
Run this in 128k mode on any ZX Spectum emulator

import os
from PIL import Image

from . import base_processor



class GreyscaleProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_image_name = ''

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_image_name = arg_value
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_image_name)

	def get_deps(self):
		return [self._src_image_name]

	def process(self):
		# load the source image
		try:
			src_image = Image.open(self._src_image_name)
		except:
			raise Exception('Couldn\'t load source image %r' %  (self._src_image_name))
		src_image = src_image.convert(mode='L')

		# gather details about the source image
		width = src_image.width
		height = src_image.height

		# create binary data
		binary_data = []
		for y in range(height):
			for x in range(width):
				output_byte = src_image.getpixel((x, y))
				binary_data.append(output_byte)

		# resource comment
		comment = 'width=%r, height=%r, src=%r' % (width, height, self._src_image_name)

		# done..
		return (binary_data, comment)

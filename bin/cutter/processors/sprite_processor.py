import os
from PIL import Image

from . import base_processor



class SpriteProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_image_name = ''
		self._dither = Image.NONE
		self._zigzag = False
		self._mask = False

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_image_name = arg_value
			elif arg_name == 'dither' and not arg_value:
				self._dither = Image.FLOYDSTEINBERG
			elif arg_name == 'zigzag' and not arg_value:
				self._zigzag = True
			elif arg_name == 'mask' and not arg_value:
				self._mask = True
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_image_name)

	def get_deps(self):
		return [self._src_image_name]
		
	def process(self):
		# load the source image
		try:
			src_image = Image.open(self._src_image_name)
		except:
			raise Exception('Couldn\'t load source image %r' %  self._src_image_name)
		rgb_image = src_image.convert(mode='RGB')
		bw_image = src_image.convert(mode='1', dither=self._dither)

		# gather details about the source image
		bytes_wide = int(src_image.width / 8)
		if bytes_wide * 8  != src_image.width:
			raise Exception('Image width of %s is not a multiple of 8' % (src_image.width))
		height = src_image.height

		# create binary data
		binary_data = []
		for y in range(height):
			reverse = self._zigzag and (y & 1)
			row = []
			for x_byte in range(bytes_wide):
				data_byte = 0
				mask_byte = 0
				for x in range(x_byte * 8, (x_byte + 1) * 8):
					r, g, b = rgb_image.getpixel((x, y))
					is_mask = (r, g, b) == (255, 0, 255)
					mask_byte = (mask_byte << 1) + (1 if is_mask else 0)
					if is_mask:
						data_pixel = 0
					else: 
						data_pixel = bw_image.getpixel((x, y))
					data_byte = (data_byte << 1) + (1 if data_pixel >= 128 else 0)
				if reverse:
					row.insert(0, data_byte)
					if self._mask:
						row.insert(0, mask_byte)
				else:
					if self._mask:
						row.append(mask_byte)
					row.append(data_byte)
			binary_data.extend(row)

		# resource comment
		comment = 'byte_width=%r, height=%r, src=%r, dither=%r, mask=%r, zigzag=%r' % (bytes_wide, height, self._src_image_name, 'false' if self._dither == Image.NONE else 'true', self._mask, self._zigzag)

		# done..
		return (binary_data, comment)

import os
from PIL import Image

from . import base_processor



class BlockProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_image_name = ''
		self._dither = Image.NONE

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_image_name = arg_value
			elif arg_name == 'dither' and not arg_value:
				self._dither = Image.FLOYDSTEINBERG
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_image_name)

	def get_deps(self):
		return [self._src_image_name]
		
	def process(self):
		# load the source image
		try:
			src_image = Image.open(self._src_image_name)
		except:
			raise Exception('Couldn\'t load source image %r' %  (self._src_image_name))
		src_image = src_image.convert(mode='1', dither=self._dither)

		# gather details about the source image
		width = src_image.width
		if width != 256:
			raise Exception('Width of image %r must be 256, not %r' % (self._src_image_name, width))
		height = src_image.height
		if height % 64 != 0:
			raise Exception('Height of image %r must be a multiple of 64, not %r' % (self._src_image_name,height))
		blocks_high = int(height / 64)

		# create binary data
		binary_data = []
		for block in range(blocks_high):
			for y in range(64):
				source_y = (block * 64) + ((y & 7) << 3) + ((y & 56) >> 3)
				for x_byte in range(32):
					output_byte = 0
					for x in range(x_byte * 8, (x_byte + 1) * 8):
						pixel = src_image.getpixel((x, source_y))
						output_byte = (output_byte << 1) + (1 if pixel >= 128 else 0)
					binary_data.append(output_byte)

		# resource comment
		comment = 'width=%r, height=%r, blocks_high=%r, src=%r, dither=%r' % (width, height, blocks_high, self._src_image_name, 'false' if self._dither == Image.NONE else 'true')

		# done..
		return (binary_data, comment)


import os

from . import base_processor



class RawProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_file_name = ''

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_file_name = arg_value
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_file_name)

	def get_deps(self):
		return [self._src_file_name]

	def process(self):
		# load the source file
		try:
			src_file = open(self._src_file_name, 'rb')
			binary_data = list(src_file.read())
		except:
			raise Exception('Couldn\'t load source file %r' %  (self._src_file_name))

		# resource comment
		comment = 'src=%r' % (self._src_file_name)

		# done..
		return (binary_data, comment)

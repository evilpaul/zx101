import abc
import time



class BaseProcessor(metaclass=abc.ABCMeta):
	def __init__(self):
		self._do_compress = True
		self._do_pin = False

	def parse_args(self, *, arg_pairs):
		remaining_args = []
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'nocompress' and not arg_value:
				self._do_compress = False
			elif arg_name == 'pin' and not arg_value:
				self._do_pin = True
			else:
				remaining_args.append(arg_pair)
		return remaining_args

	def get_resource_timestamp(self):
		return time.time()

	def get_deps(self):
		return []

	@abc.abstractmethod
	def process(self):
		pass

	def should_compress(self):
		return self._do_compress

	def should_pin(self):
		return self._do_pin

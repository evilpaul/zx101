import math

from . import base_processor



class SinProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._length = 256
		self._minimum = -128
		self._maximum = 127

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'minimum' and arg_value:
				self._minimum = int(arg_value)
			elif arg_name == 'maximum' and arg_value:
				self._maximum = int(arg_value)
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def process(self):
		# generate the sine table
		binary_data = []
		full_range = self._maximum - self._minimum
		for i in range(self._length):
			value = math.sin(math.radians(i / 256 * 360))
			value = ((value + 1) / 2) * full_range + self._minimum
			value = int(value)
			if value < 0:
				value = value + 256
			binary_data.append(value)

		# resource comment
		comment = 'length=%r, range=%r to %r' % (self._length, self._minimum, self._maximum)

		# done..
		return (binary_data, comment)

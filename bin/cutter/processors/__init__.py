from . import base_processor

from . import fourbyfour_processor
from . import block_processor
from . import colourblock_processor
from . import greyscale_processor
from . import raw_processor
from . import sin_processor
from . import sprite_processor
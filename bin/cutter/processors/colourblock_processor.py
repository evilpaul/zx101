import math
import os
from PIL import Image

from . import base_processor



class ColourBlockProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_image_name = ''
		self._dither = Image.NONE

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_image_name = arg_value
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_image_name)

	def get_deps(self):
		return [self._src_image_name]
		
	def process(self):
		# load the source image
		try:
			src_image = Image.open(self._src_image_name)
		except:
			raise Exception('Couldn\'t load source image %r' %  (self._src_image_name))

		# gather details about the source image
		width = src_image.width
		if width != 256:
			raise Exception('Width of image %r must be 256, not %r' % (self._src_image_name, width))
		height = src_image.height
		if height % 64 != 0:
			raise Exception('Height of image %r must be a multiple of 64, not %r' % (self._src_image_name,height))
		blocks_high = int(height / 64)

		# extract the colour information for all chars
		attribute_data = []
		for y in range(blocks_high * 8):
			for x in range(32):
				char_attributes = []
				for py in range(y*8, (y+1)*8):
					for px in range(x*8, (x+1)*8):
						pixel = self._nearest_palette_index(src_image.getpixel((px, py)))
						if pixel not in char_attributes:
							char_attributes.append(pixel)
				if len(char_attributes) == 1:
					attribute_data.append((char_attributes[0], char_attributes[0], True))
				elif len(char_attributes) == 2:
					attribute_data.append((char_attributes[0], char_attributes[1], True))
				else:
					# more than two colours in this block
					attribute_data.append((char_attributes[0], char_attributes[1], False))

		# create binary data
		binary_data = []

		# first the pixel data..
		for block in range(blocks_high):
			for y in range(64):
				source_y = (block * 64) + ((y & 7) << 3) + ((y & 56) >> 3)
				for x_byte in range(32):
					attribute = attribute_data[x_byte + int(source_y/8) * 32 ]
					output_byte = 0
					for x in range(x_byte * 8, (x_byte + 1) * 8):
						pixel = self._nearest_palette_index(src_image.getpixel((x, source_y)))
						on_off = 0 if pixel == attribute[0] else 1
						output_byte = (output_byte << 1) | on_off
					binary_data.append(output_byte)

		# ..then the attributes
		for attribute in attribute_data:
			valid = attribute[2]
			if valid:
				ink = attribute[0]
				paper = attribute[1]
				final_attribute_value = ((ink & 7) << 3) | (paper & 7) | (64 if ((ink|paper)&8) else 0)
				binary_data.append(final_attribute_value)
			else:
				# highlight colour errors as black and white flashing
				binary_data.append(199)

		# resource comment
		comment = 'width=%r, height=%r, blocks_high=%r, src=%r' % (width, height, blocks_high, self._src_image_name)

		# done..
		return (binary_data, comment)

	def _nearest_palette_index(self, rgb):
		palette = [
			(0,0,0),
			(0,0,190),
			(190,0,0),
			(190,0,190),
			(0,190,0),
			(0,190,190),
			(190,190,0),
			(190,190,190),
			(0,0,0),
			(0,0,255),
			(255,0,0),
			(255,0,255),
			(0,255,0),
			(0,255,255),
			(255,255,0),
			(255,255,255)]
		best_i = 0
		best_diff = math.inf
		for i in range(len(palette)):
			palette_rgb = palette[i]
			diff = math.pow(rgb[0] - palette_rgb[0], 2) + math.pow(rgb[1] - palette_rgb[1], 2) + math.pow(rgb[2] - palette_rgb[2], 2)
			if diff < best_diff:
				best_diff = diff
				best_i = i 
		return best_i
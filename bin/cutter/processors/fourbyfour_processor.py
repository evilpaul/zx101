import os
import math
from PIL import Image

from . import base_processor



class FourByFourProcessor(base_processor.BaseProcessor):
	def __init__(self):
		super().__init__()
		self._src_image_name = ''

	def parse_args(self, *, arg_pairs):
		arg_pairs = super().parse_args(arg_pairs=arg_pairs)
		for arg_pair in arg_pairs:
			arg_name, arg_value = arg_pair
			if arg_name == 'src' and arg_value:
				self._src_image_name = arg_value
			else:
				raise Exception('Unknown or incomplete arg \'%s=%s\'' % (arg_pair[0], arg_pair[1]))
		return []

	def get_resource_timestamp(self):
		return os.path.getmtime(self._src_image_name)

	def get_deps(self):
		return [self._src_image_name]

	def process(self):
		# load the source image
		try:
			src_image = Image.open(self._src_image_name)
		except:
			raise Exception('Couldn\'t load source image %r' %  (self._src_image_name))
		src_image = src_image.convert(mode='RGB')

		# gather details about the source image
		width = src_image.width
		if width & 1:
			raise Exception('Image width of %s is not a multiple of 2' % (width))
		height = src_image.height

		# create binary data
		binary_data = []
		for y in range(height):
			for x in range(0, width, 2):
				p1 = self._nearest_palette_index(src_image.getpixel((x+0, y)))
				p2 = self._nearest_palette_index(src_image.getpixel((x+1, y)))
				data = ((p1&7)<<3) + (p2&7) + (((p1&8) | (p2&8))<<3)
				binary_data.append(data)

		# resource comment
		comment = 'src=%r' % (self._src_image_name)

		# done..
		return (binary_data, comment)

	def _nearest_palette_index(self, rgb):
		palette = [
			(0,0,0),
			(0,0,85),
			(85,0,0),
			(85,0,85),
			(0,85,0),
			(0,85,85),
			(85,85,0),
			(85,85,85),
			(0,0,0),
			(0,0,255),
			(255,0,0),
			(255,0,255),
			(0,255,0),
			(0,255,255),
			(255,255,0),
			(255,255,255)]
		best_i = 0
		best_diff = math.inf
		for i in range(len(palette)):
			palette_rgb = palette[i]
			diff = math.pow(rgb[0] - palette_rgb[0], 2) + math.pow(rgb[1] - palette_rgb[1], 2) + math.pow(rgb[2] - palette_rgb[2], 2)
			if diff < best_diff:
				best_diff = diff
				best_i = i 
		return best_i
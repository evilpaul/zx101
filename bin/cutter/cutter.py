import argparse
import datetime
import os
import subprocess
import sys
import traceback

import processors


def gather_resources(*, command_script, processors):
	resources = {}

	# Get processors for each resource
	line_number = 0
	for command in command_script:
		line_number = line_number + 1
		args = command.lstrip().partition('#')[0].split()
		if len(args) == 0:
			# comment or blank line
			continue
		elif len(args) < 2:
			# must have at least two arguments
			print('Command on line %r needs at least two arguments' % (line_number))
			exit()
		else:
			resource_name = args[0]
			resource_type = args[1]

			if resource_name in resources:
				raise Exception('Resource name %r on line %r already used' % (resource_name, line_number))
			if len(resource_name) > 32:
				raise Exception('Resource name %r on line %r must be 32 characters or less' % (resource_name, line_number))

			try:
				if resource_type in processors:
					arg_pairs = []
					for arg in args[2:]:
						arg = arg.partition('=') 
						arg_pairs.append([arg[0], arg[2]]) 
					resources[resource_name] = {
						'processor': processors[resource_type](),
						'resource_type': resource_type,
						'line_number': line_number,
						'arg_pairs': arg_pairs,
						'comment': None,
						'bindary_data': None,
					}
				else:
					raise Exception('Unknown type %r' % (resource_type))
			except Exception as e:
				traceback.print_exc()
				raise Exception('Error creating %r type %r on line %r: %s' % (resource_name, resource_type, line_number, e))

	# Parse resource arguments
	for resource_name in resources:
		resource = resources[resource_name]
		try:
			resource['processor'].parse_args(arg_pairs=resource['arg_pairs'])
		except Exception as e:
			traceback.print_exc()
			raise Exception('Error reading %r arguments on line %r: %s' % (resource_name, resource['line_number'], e))

	return resources


def process_resources(*, resources):
	# Process resources
	for resource_name in resources:
		resource = resources[resource_name]
		try:
			(binary_data, comment) = resource['processor'].process()
		except Exception as e:
			traceback.print_exc()
			raise Exception('Error processing %r on line %r: %s' % (resource_name, resource['line_number'], e))
		comment = '%s %s' % (resource['resource_type'], comment)
		resources[resource_name]['bindary_data'] = binary_data
		resources[resource_name]['comment'] = comment

	return resources


def get_deps(*, resources):
	deps = []
	for resource_name in resources:
		resource = resources[resource_name]
		resource_deps = resource['processor'].get_deps()
		for dep in resource_deps:
			if dep not in deps:
				deps.append(dep)
	return deps


def compress_resources(*, resources):
	# write details for each resource
	for resource_name in resources:
		resource = resources[resource_name]
		binary_data = resource['bindary_data']
		original_length = len(binary_data)

		# compress 
		do_compression = resource['processor'].should_compress()
		compression_type = 0
		if do_compression:
			resource_timestamp = resource['processor'].get_resource_timestamp()
			compressed_binary_data = compress(resource_name, resource_timestamp, binary_data)
			compressed_length = len(compressed_binary_data)
			if compressed_length < original_length:
				binary_data = compressed_binary_data
				compression_type = 1

		resource['original_length'] = original_length
		resource['compression_type'] = compression_type
		resource['bindary_data'] = binary_data

	return resources


def bank_resources(*, resources):
	banks = {}
	for bank in [0, 1, 3, 4]:
		banks[bank] = {
			'bytes_left': 16384,
			'resource_list': []
		}

	# start with any pinned resources
	for resource_name in resources:
		resource = resources[resource_name]

		# only worry about pinned resources
		if resource['processor'].should_pin():
			binary_data = resource['bindary_data']
			compressed_length = len(binary_data)

			# find the next empty bank that we can pin to
			resource_banked = False
			for bank_index in banks:
				bank = banks[bank_index]
				if not bank['resource_list'] and bank['bytes_left'] - compressed_length > 0:
					# add resource to bank
					resource['bank'] = bank_index
					resource['was_pinned'] = True
					bank['resource_list'].append(resource_name)
					bank['bytes_left'] = bank['bytes_left'] - compressed_length
					resource_banked = True
					break
			if not resource_banked:
				raise Exception('Cannot find available bank to pin %r (%d bytes long)' % (resource_name, compressed_length))

	# fit all of the resources into banks on a first come, first served basis
	for resource_name in resources:
		resource = resources[resource_name]

		# only worry about non-pinned resources
		if not resource['processor'].should_pin():
			binary_data = resource['bindary_data']
			compressed_length = len(binary_data)

			# find the next bank that has enough space to fit this resource
			resource_banked = False
			for bank_index in banks:
				bank = banks[bank_index]
				if bank['bytes_left'] - compressed_length > 0:
					# add resource to bank
					resource['bank'] = bank_index
					resource['was_pinned'] = False
					bank['resource_list'].append(resource_name)
					bank['bytes_left'] = bank['bytes_left'] - compressed_length
					resource_banked = True
					break
			if not resource_banked:
				raise Exception('Cannot find available bank to fit %r (%d bytes long)' % (resource_name, compressed_length))

	return banks, resources


def write_resources(*, script_name, resources, banks, dict_filename, binary_filename_base):
	try:
		dict_file = open(dict_filename, 'wt')
	except:
		raise Exception('Couldn\'t open %r for output' % (dict_filename))

	dict_header = \
		';                __    __\n' + \
		';   ____  __ ___/  |__/  |_  ___________ \n' + \
		'; _/ ___\|  |  \   __\   __\/ __ \_  __ \\\n' + \
		'; \  \___|  |  /|  |  |  | \  ___/|  | \/\n' + \
		';  \___  >____/ |__|  |__|  \___  >__|\n' + \
		';      \/                       \/\n' + \
		';\n' + \
		'; cutter header output from %r\n' % (script_name) + \
		'; generated at %s\n' % (datetime.datetime.now().isoformat()) + \
		'; %r resources found\n' % (len(resources))

	dict_body = \
		'resource_names\n'

	dict_footer = \
		'	db	-1\n' + \
		'\n' + \
		'	; bank,compression_type ; id: name\n' + \
		'	; address,compressed_length\n' + \
		'resource_details\n'

	resource_id = 0
	total_data_length = 0
	total_compressed_data_length = 0

	# write out each bank
	for bank_index in banks:
		bank = banks[bank_index]

		# open binary file to write to
		binary_filename = '%s.%d.bin' % (binary_filename_base, bank_index)
		try:
			binary_file = open(binary_filename, 'wb')
		except:
			raise Exception('Couldn\'t open %r for output' % (binary_filename))
		base_address = 16384*3

		# write bank details to header
		bank_bytes_left = 16384 - bank['bytes_left']
		dict_header = dict_header + '; bank %d used %d/16384 bytes (%.2f%% full)\n' % (bank_index, bank_bytes_left, (bank_bytes_left / 16384) * 100)

		# write details for each resource
		for resource_name in bank['resource_list']:
			resource = resources[resource_name]
			binary_data = resource['bindary_data']
			comment = resource['comment']
			original_length = resource['original_length']
			compressed_length = len(binary_data)
			compression_type = resource['compression_type']
			was_pinned = resource['was_pinned']

			# write resource name to dict data
			dict_body = dict_body + '	db %r%s ; %s\n' % (resource_name, ',0', comment)

			# write resource details to dict data
			dict_footer = dict_footer + '	db %r,%r ; %r: %s' % (bank_index, compression_type, resource_id, resource_name)
			if compression_type == 0:
				dict_footer = dict_footer + ' (no compression)'
			if was_pinned:
				dict_footer = dict_footer + ' (was pinned)'
			dict_footer = dict_footer + '\n	dw %r,%r\n' % (base_address, compressed_length)

			# write the compressed binary data
			binary_file.write(bytes(binary_data))

			total_data_length = total_data_length + original_length
			total_compressed_data_length = total_compressed_data_length + compressed_length
			base_address = base_address + compressed_length
			resource_id = resource_id + 1

	# finish the header
	dict_header = dict_header + \
		'; total data size: %r(compressed) %r(uncompressed)\n' % (total_compressed_data_length, total_data_length) + \
		'; compressed data is %.2f%% the size of uncompressed data\n' % ((total_compressed_data_length / total_data_length) * 100) + \
		'\n' + \
		'\n'

	# write the dict file now
	dict_file.write(dict_header + dict_body + dict_footer)


def compress(resource_name, resource_timestamp, in_data):
	compressed_filename = 'build/data/%s.compressed' % ( resource_name)

	# Has the file changed since it was last compressed?
	need_to_compress = True
	try:
		compressed_timestamp = os.path.getmtime(compressed_filename)
		if resource_timestamp < compressed_timestamp:
			need_to_compress = False
	except:
		pass

	# Make a new compressed file if we need to
	if need_to_compress:
		print('Compressing resource: %s' % (resource_name))

		# Write binary file to a temporary file
		uncompressed_temp = open('build/data/uncompressed_temp', 'wb')
		uncompressed_temp.write(bytes(in_data))
		uncompressed_temp.close()

		# Compress using external compressor
		subprocess.call(['bin/megalz', 'build/data/uncompressed_temp', compressed_filename])
	else:
		print('Skipping compressing unchanged resource: %s' % (resource_name))

	# Read in compressed data
	compressed_file = open(compressed_filename, 'rb')
	compressed_data = list(compressed_file.read())
	compressed_file.close()
	
	return compressed_data


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Spare us the cutter')
	parser.add_argument('script_filename')
	parser.add_argument('output_directory')
	parser.add_argument('-md', type=str, default=None)
	args = parser.parse_args()

	try:
		script_file = open(args.script_filename, 'r')
		commands = script_file.readlines()
		script_file.close()
	except:
		raise Exception('Couldn\'t open %r for input' % (args.script_filename))
		exit(-1)

	processors = {
		'4x4': processors.fourbyfour_processor.FourByFourProcessor,
		'block': processors.block_processor.BlockProcessor,
		'colourblock': processors.colourblock_processor.ColourBlockProcessor,
		'greyscale': processors.greyscale_processor.GreyscaleProcessor,
		'raw': processors.raw_processor.RawProcessor,
		'sin': processors.sin_processor.SinProcessor,
		'sprite': processors.sprite_processor.SpriteProcessor,
	}

	try:
		resources = gather_resources(command_script=commands, processors=processors)
		if args.md:
			deps = get_deps(resources=resources)
			try:
				deps_file = open(args.md, 'wt')
			except:
				raise Exception('Couldn\'t open %r for output' % (args.md))
			deps_file.write('%s\cutter.0.bin : %s' % (args.output_directory, ' '.join(deps)))
		else:
			resource_list = process_resources(resources=resources)
			resources = compress_resources(resources=resources)
			banks, resources = bank_resources(resources=resources)
			write_resources(script_name=args.script_filename, resources=resources, banks=banks, dict_filename=args.output_directory + '\cutter.dict', binary_filename_base=args.output_directory + '\cutter')
	except Exception as e:
		print(e)
		exit(-1)


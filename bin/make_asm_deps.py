import argparse
import re


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Make dependency files from asm files')
	parser.add_argument('input_filename')
	parser.add_argument('output_filename')
	parser.add_argument('target_filename')
	args = parser.parse_args()
	print('Make deps from %r to %r, target %r' % (args.input_filename, args.output_filename, args.target_filename))

	try:
		input_file = open(args.input_filename, 'rt')
	except:
		raise Exception('Couldn\'t open input file %r' % (args.input_filename))


	dependencies = [args.input_filename]
	for line in input_file.readlines():
		m = re.search('include\W+([\w\\\\.]+)', line )
		if m:
			dependencies.append(m.groups()[0])

	try:
		output_file = open(args.output_filename, 'wt')
	except:
		raise Exception('Couldn\'t open output file %r' % (args.output_filename))


	output_file.write(args.target_filename + ' :')
	for dependency in dependencies:
		output_file.write(' ' + dependency)
	output_file.write('\n')

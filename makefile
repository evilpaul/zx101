default : all
.phony : all clean make_directories make_modules

MODULES=setcolours tunnel gears twister sprites bigspinner plasma ground multicolour_pic multicolour_twister screen music
BANKS=0 1 3 4

BUILD_DIRECTORIES=build build\data build\modules deps deps\modules
MODULE_DEPS=$(foreach module,$(MODULES),deps\modules\$(module).d)
MODULE_TARGETS=$(foreach module,$(MODULES),build\modules\$(module).bin)
BANK_DEPS=$(foreach bank,$(BANKS),deps\bank.$(bank).d)
BANK_TARGETS=$(foreach bank,$(BANKS),build\bank.$(bank).tap)
CUTTER_OUTPUTS=build\data\cutter.dict $(foreach bank,$(BANKS),build\data\cutter.$(bank).bin)

COPY=copy $*
CUTTER=@bin\cutter\cutter.py
MAKE_ASM_DEPS=@bin\make_asm_deps.py
DELETE=@DEL /Q $*
DELETE_DIR=@rmdir /Q /S $*
ECHO=@echo
MKDIR=@mkdir
PASMO=bin\pasmo.exe
SHELL=cmd.exe


-include deps/*.d
-include deps/modules/*.d


all : make_deps make_directories build\demo.tap


clean : clean_deps clean_code

clean_code :
	$(DELETE_DIR) build


make_deps : make_directories $(MODULE_DEPS) make_cutter_deps
$(MODULE_DEPS) :
	$(MAKE_ASM_DEPS) $(subst .d,.asm,$(subst deps,source,$@)) $@ $(subst .d,.bin,$(subst deps,build,$@))
make_cutter_deps :
	$(CUTTER) data\cutter_script.txt build\data -md deps\cutter.0.d

clean_deps : 
	$(DELETE_DIR) deps


make_directories : $(BUILD_DIRECTORIES)
$(BUILD_DIRECTORIES) :
	$(MKDIR) $@


make_modules : $(MODULE_TARGETS)
$(MODULE_TARGETS) :
	$(PASMO) --alocal $< $@


make_banks : $(BANK_TARGETS)
$(BANK_TARGETS) :
	$(COPY) build\data\cutter.$(subst .tap,,$(subst build\bank.,,$@)).bin build\data\cutter.temp.bin
	$(PASMO) --tap --alocal --equ BANK_NUMBER=$(subst .tap,,$(subst build\bank.,,$@)) source\bank_loader.asm build\bank.$(subst .tap,,$(subst build\bank.,,$@)).tap
	$(DELETE) build\data\cutter.temp.bin


$(CUTTER_OUTPUTS) : data\cutter_script.txt $(MODULE_TARGETS)
	$(CUTTER) data\cutter_script.txt build\data
	

build\main.tap deps\main.d : $(CUTTER_OUTPUTS)
	$(PASMO) --tap --alocal source\main.asm build\main.tap


build\demo.tap : build\main.tap $(BANK_TARGETS) data\boot_stub.tap
	$(COPY) data\boot_stub.tap /B + build\bank.?.tap /B + build\main.tap /B build\demo.tap

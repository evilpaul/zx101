include		'source\utils\defines.asm'

SYSTEM_RELEASE EQU "1"


			org		32768-2048

entry		jp		start


LOAD_MODULE	macro ?resource_name
		db	0,?resource_name,0
endm
WAIT macro ?frames
		db	2
		dw	?frames
endm
INSTALL_USER_TOP_VBL macro ?address
		db	4
		dw	?address
endm
INSTALL_USER_VBL macro ?resource_name
		db	6,?resource_name,0
endm
SET_D macro ?value
		db	8,?value
endm
SET_E macro ?value
		db	10,?value
endm
SET_DE macro ?value
		db	12
		dw	?value
endm
SET_H macro ?value
		db	14,?value
endm
SET_L macro ?value
		db	16,?value
endm
SET_HL macro ?value
		db	18
		dw	?value
endm
STOP macro
		db	20
endm
END_OF_SCRIPT_COMMANDS	equ	22



script
			include	'source\script.asm'

			include	'build\data\cutter.dict'


IF $ >= (32768-50)
	IF $ >= (32768)
		.ERROR !!!! Lower system code too large
	ELSE
		.WARNING !!!! Lower system code near end of available space
	ENDIF
ENDIF



ALIGN_AT	32768
swap_on_vsync
			ld		hl,do_swap_flag
			ld		(hl),h
			ret

pull_resource_script
_loop		ld		a,(hl)
			or		a
			ret		z
			push	hl
			xor		a
			ld		b,-1
			cpir
			ld		e,(hl)
			inc		hl
			ld		d,(hl)
			inc		hl
			ld		(_restore+1),hl
			pop		hl
			call	pull_resource
_restore	ld		hl,1234
			jr		_loop

; hl -> source string
; de = dest
pull_resource
			push	bc
			push	de
			push	hl

			push	de						; save destination
			call	find_resource
			or		a
			jr		z,_resource_not_found

			; swap to bank
			push	bc						; save length
			di
			ld		a,(paging_value)
			and		%11111000
			or		e						; mix with resource bank
			ld		(paging_value),a
			ld		bc,PAGING_PORT
			out		(c),a
			ei
			pop		bc						; restore length

			; copy/uncompress data to destination
			ld		a,d						; save compression type
			pop		de						; restore destination address
			or		a
			jr		nz,_decomp
_copy		call	fast_ldir
			jr		_got_data
_decomp		call	mlz_decomp
_got_data

			; page screen back in
			di
			ld		a,(paging_value)
			and		%11111000
			ld		b,a
			cpl
			rrca
			rrca
			and		%00000010
			or		%00000101
			or		b
			ld		(paging_value),a
			ld		bc,PAGING_PORT
			out		(c),a
			ei

			pop		hl
			pop		de
			pop		bc
			ret

_resource_not_found
			ld		a,'R'
			jp		show_error


; in:
;	hl = source string
; out:
;	a = zero if not found
;	hl = source
;	bc = length
;	d = compression type
;	e = bank
find_resource
			ex		af,af'
			xor		a						; resource index in here
			ex		af,af'
			ld		de,resource_names
_search		push	hl
			ld		a,(de)
			cp		-1
			jr		nz,_continue_search
			pop		hl
			xor		a						; return 'resource not found'
			ret		z
_continue_search
			ld		a,(de)
			or		(hl)					; at end of both source and search strings?
			jr		z,_found_resource
			ld		a,(de)
			cp		(hl)
			jr		nz,_next
			inc		hl
			inc		de
			jr		_continue_search
_next		ld		a,(de)					; search for the start of the next resource name
			inc		de
			or		a
			jr		nz,_next
			pop		hl						; restore pointer to source string
			ex		af,af'
			inc		a						; increase resource index count
			ex		af,af'
			jr		_search
_found_resource
			pop		af
			ex		af,af'					; resource index is in a

			; point hl at resource details (6 bytes for each resource)
			ld		b,a
			add		a,a						; *2
			add		a,b						; *3
			ld		h,0
			ld		l,a
			add		hl,hl					; *6
			ld		bc,resource_details
			add		hl,bc

			; grab bank into e
			ld		e,(hl)
			inc		hl

			; grab compression type into d
			ld		d,(hl)
			inc		hl

			; grab source address of compressed data in bc
			ld		c,(hl)
			inc		hl
			ld		b,(hl)
			inc		hl

			; grab length in hl
			ld		a,(hl)
			inc		hl
			ld		h,(hl)
			ld		l,a

			; put source into hl, length into bc and bak/compression into de
			push	bc
			push	hl
			pop		bc
			pop		hl
			ld		a,1						; return 'resource found'
			ret


interrupt_routine
			push	af
			ex		af,af'
			push	af
			push	bc
			push	de
			push	hl
			exx
			push	bc
			push	de
			push	hl
			push	ix
			push	iy

			ld		hl,(user_top_vbl)
			ld		a,h
			or		l
			jr		z,_no_top_vbl
			ld		a,(paging_value)
			and		%11111000
			ld		b,a
			rrca
			rrca
			and		%00000010
			or		%00000101
			or		b
			ld		bc,PAGING_PORT
			out		(c),a
			ld		de,_done_top_vbl
			push	de
			jp		(hl)
_done_top_vbl
			ld		a,(paging_value)
			ld		bc,PAGING_PORT
			out		(c),a
_no_top_vbl

			ld		a,1
			out		(BORDER_PORT),a

			; wait timer
			ld		bc,(script_wait)
			ld		a,b
			or		c
			jr		z,_waited
			dec		bc
			ld		(script_wait),bc
_waited

			; flip screens?
			ld		a,(do_swap_flag)
			or		a
			jr		z,_no_swap
			xor		a
			ld		(do_swap_flag),a
			ld		a,(paging_value)
			xor		%00001000
			ld		(paging_value),a
			ld		bc,PAGING_PORT
			out		(c),a
_no_swap

			; user vbl?
			ld		a,(do_user_vbl_flag)
			or		a
			jr		z,_no_user_vbl
			ld		a,(user_vbl_bank)
			and		%00000111
			ld		b,a
			ld		a,(paging_value)
			and		%11111000
			or		b
			ld		bc,PAGING_PORT
			out		(c),a
			ld		hl,user_vbl_address
			ld		de,_call+1
			ldi
			ldi
_call		call	1234
			ld		a,(paging_value)
			ld		bc,PAGING_PORT
			out		(c),a
_no_user_vbl

			ld		a,7
			out		(BORDER_PORT),a

			pop		iy
			pop		ix
			pop		hl
			pop		de
			pop		bc
			exx
			pop		hl
			pop		de
			pop		bc
			pop		af
			ex		af,af'
			pop		af

			ei
			ret


do_swap_flag		db		0
do_user_vbl_flag	db		0
user_vbl_bank		db		0
user_vbl_address	dw		0
user_top_vbl		dw		0

paging_value		db		0

			include	'source\utils\fast_ldir.asm'
			include	'source\utils\mlz.asm'


start
			; set up..
			di
			ld		sp,stack

			; set up interrupt routine
			ld		a,37
			ld		i,a
			ld		de,interrupt_routine
			ld		hl,23670
			ld		(hl),$c3						; opcode for 'jp nn'
			inc		hl
			ld		(hl),e
			inc		hl
			ld		(hl),d
			im		2

			; set up the initial paging
			ld		a,(0x5b5c)
			and		%11110000
			or		%00001101
			ld		(paging_value),a
			ld		bc,PAGING_PORT
			out		(c),a

			; and we're ready to start..
			ei

main_loop
			; sync
			ld		a,0
			out		(BORDER_PORT),a
			halt
			ld		a,2
			out		(BORDER_PORT),a

			; do script
			ld		hl,(script_wait)
			ld		a,h
			or		l
			jp		nz,_run_command
_script_ptr	ld		hl,script
_next_command
			ld		a,(hl)
			inc		hl
			cp		END_OF_SCRIPT_COMMANDS
			jr		nc,_command_error
			push	hl
			ld		h,0
			ld		l,a
			ld		de,_command_jt
			add		hl,de
			ld		de,_jump+1
			ldi
			ldi
			pop		hl
_jump		jp		1234
_command_error
			ld		a,'S'
			jp		show_error
_command_jt	dw		_command_load_module
			dw		_command_wait
			dw		_command_install_user_top_vbl
			dw		_command_install_user_vbl
			dw		_command_set_d
			dw		_command_set_e
			dw		_command_set_de
			dw		_command_set_h
			dw		_command_set_l
			dw		_command_set_hl
			dw		_command_stop

_command_load_module
			push	hl
_msearch	ld		a,(hl)
			or		a
			inc		hl
			jr		z,_mend
			jr		_msearch
_mend		ld		(_script_ptr+1),hl
			pop		hl

			ld		de,32768+1024
			call	pull_resource

			xor		a								; reset call counter
			ld		(_set_bc+1),a
			ld		(_set_bc+2),a
			jp		_run_command
_command_wait
			ld		e,(hl)
			inc		hl
			ld		d,(hl)
			inc		hl
			ld		(_script_ptr+1),hl
			ld		(script_wait),de
			jr		_run_command
_command_install_user_top_vbl
			ld		e,(hl)
			inc		hl
			ld		d,(hl)
			inc		hl
			ld		(_script_ptr+1),hl
			ld		(user_top_vbl),de
			jr		_run_command
_command_install_user_vbl
			di
			push	hl								; save pointer to resource name
_vsearch	ld		a,(hl)							; search for next command start in de
			or		a
			inc		hl
			jr		z,_vend
			jr		_vsearch
_vend		ex		de,hl
			pop		hl								; pointer to resource name back in hl
			push	de								; save address of next command
			call	find_resource
			or		a
			jr		z,_vbl_error
			ld		a,e								; check that user_vbl is not compressed
			or		a
			jr		z,_user_vbl_ok
_vbl_error	ld		a,'V'
			jp		show_error
_user_vbl_ok
			ld		a,e								; save bank
			ld		(user_vbl_bank),a
			ex		de,hl							; save address
			ld		hl,user_vbl_address
			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		a,1								; signal that user_vbl is available
			ld		(do_user_vbl_flag),a
			pop		hl								; restore address of next command
			ei
			jp		_next_command
_command_set_d
			ld		a,(hl)
			inc		hl
			ld		(_set_de+2),a
			jp		_next_command
_command_set_e
			ld		a,(hl)
			inc		hl
			ld		(_set_de+1),a
			jp		_next_command
_command_set_de
			ld		a,(hl)
			inc		hl
			ld		(_set_de+1),a
			ld		a,(hl)
			inc		hl
			ld		(_set_de+2),a
			jp		_next_command
_command_set_h
			ld		a,(hl)
			inc		hl
			ld		(_set_hl+2),a
			jp		_next_command
_command_set_l
			ld		a,(hl)
			inc		hl
			ld		(_set_hl+1),a
			jp		_next_command
_command_set_hl
			ld		a,(hl)
			inc		hl
			ld		(_set_hl+1),a
			ld		a,(hl)
			inc		hl
			ld		(_set_hl+2),a
			jp		_next_command
_command_stop
			di
			halt


_run_command
			; page screen back in
			di
			ld		a,(paging_value)
			and		%11111000
			ld		b,a
			ld		a,(paging_value)
			cpl
			rrca
			rrca
			and		%00000010
			or		%00000101
			or		b
			ld		(paging_value),a
			ld		bc,PAGING_PORT
			out		(c),a
			ei

			ld		a,3
			out		(BORDER_PORT),a
_set_bc		ld		bc,0
_set_de		ld		de,0
_set_hl		ld		hl,0
			call	32768+1024
			ld		bc,(_set_bc+1)					; increase call counter
			inc		bc
			ld		(_set_bc+1),bc

			jp		main_loop

script_wait
			dw		0

			ds		30*2
stack		dw		0

show_error
			di
			
			ld		de,CHARSET
			ld		h,e
			ld		l,a
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,de
			ld		d,HIGH(16384)
			ld		b,8
_show_code	ld		a,(hl)
			ld		(de),a
			inc		l
			inc		d
			djnz	_show_code

			ld		hl,16384+2048*3
			ld		(hl),COLOUR_BG_RED|COLOUR_FG_WHITE|COLOUR_BRIGHT|COLOUR_FLASH

_l			out		(BORDER_PORT),a
			inc		a
			jr		_l




IF $ >= (MODULE_START-32-50)
	IF $ >= (MODULE_START-32)
		.ERROR !!!! Upper system code too large
	ELSE
		.WARNING !!!! Upper system code near end of available space
	ENDIF
ENDIF



ALIGN_AT	MODULE_START-32
			db "=101 - AteBit Demo System - R.",SYSTEM_RELEASE,"="
ALIGN_AT	MODULE_START
			ret


end entry


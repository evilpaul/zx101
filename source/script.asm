

			INSTALL_USER_VBL		'music'

;			LOAD_MODULE				'mod_bigspinner'
;			WAIT					300
			
			LOAD_MODULE				'mod_ground'
			WAIT					300

			SET_DE					COLOUR_FG_GREEN|COLOUR_BG_BLACK
			LOAD_MODULE				'mod_setcolours'
			WAIT					5
			LOAD_MODULE				'mod_tunnel'
			WAIT					300

			SET_DE					COLOUR_FG_BLACK|COLOUR_BG_BLUE
			LOAD_MODULE				'mod_setcolours'
			WAIT					5
			LOAD_MODULE				'mod_twister'
			WAIT					300

			SET_DE					COLOUR_FG_YELLOW|COLOUR_BG_RED
			LOAD_MODULE				'mod_setcolours'
			WAIT					5
			LOAD_MODULE				'mod_gears'
			WAIT					300

			LOAD_MODULE				'mod_multicolour_pic'
			WAIT					10
			INSTALL_USER_TOP_VBL	32768+1024+3
			WAIT					300
			INSTALL_USER_TOP_VBL	0

			LOAD_MODULE				'mod_multicolour_twister'
			WAIT					10
			INSTALL_USER_TOP_VBL	32768+1024+3
			WAIT					300
			INSTALL_USER_TOP_VBL	0
			
			SET_DE					COLOUR_FG_WHITE|COLOUR_BG_BLACK
			LOAD_MODULE				'mod_setcolours'
			WAIT					5
			LOAD_MODULE				'mod_sprites'
			WAIT					300

			LOAD_MODULE				'mod_plasma'
			WAIT					300

			LOAD_MODULE				'mod_screen'
			WAIT					300
			
			STOP



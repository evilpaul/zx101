;Z80 depacker for megalz V4 packed files   (C) fyrex^mhm

; DESCRIPTION:
;
; Depacker is fully relocatable, not self-modifying,
;it's length is 110 bytes starting from DEC40.
;Register usage: AF,AF',BC,DE,HL. Must be CALL'ed, return is done by RET.
;Provide extra stack location for store 2 bytes (1 word). Depacker does not
;disable or enable interrupts, as well as could be interrupted at any time
;(no f*cking wicked stack usage :).

; USAGE:
;
; - put depacker anywhere you want,
; - put starting address of packed block in HL,
; - put location where you want data to be depacked in DE,
;   (much like LDIR command, but without BC)
; - make CALL to depacker (DEC40).
; - enjoy! ;)

; PRECAUTIONS:
;
; Be very careful if packed and depacked blocks coincide somewhere in memory.
;Here are some advices:
;
; 1. put packed block to the highest addresses possible.
;     Best if last byte of packed block has address #FFFF.
;
; 2. Leave some gap between ends of packed and depacked block.
;     For example, last byte of depacked block at #FF00,
;     last byte of packed block at #FFFF.
;
; 3. Place nonpackable data to the end of block.
;
; 4. Always check whether depacking occurs OK and neither corrupts depacked data
;     nor hangs computer.
;

mlz_decomp
        ld      a,#80
        ex      af,af'
ms      ldi
m0      ld      bc,#2ff
m1      ex      af,af'
m1x     add     a,a
        jr      nz,m2
        ld      a,(hl)
        inc     hl
        rla
m2      rl      c
        jr      nc,m1x
        ex      af,af'
        djnz    x2
        ld      a,2
        sra     c
        jr      c,n1
        inc     a
        inc     c
        jr      z,n2
        ld      bc,#33f
        jr      m1

x2      djnz    x3
        srl     c
        jr      c,ms
        inc     b
        jr      m1
x6
        add     a,c
n2
        ld      bc,#4ff
        jr      m1
n1
        inc     c
        jr      nz,m4
        ex      af,af'
        inc     b
n5      rr      c
        ret     c
        rl      b
        add     a,a
        jr      nz,n6
        ld      a,(hl)
        inc     hl
        rla
n6      jr      nc,n5
        ex      af,af'
        add     a,b
        ld      b,6
        jr      m1
x3
        djnz    x4
        ld      a,1
        jr      m3
x4      djnz    x5
        inc     c
        jr      nz,m4
        ld      bc,#51f
        jr      m1
x5
        djnz    x6
        ld      b,c
m4      ld      c,(hl)
        inc     hl
m3      dec     b
        push    hl
        ld      l,c
        ld      h,b
        add     hl,de
        ld      c,a
        ld      b,0
        ldir
        pop     hl
        jr      m0
;END_DEC40


		ALIGN_BY	256
line_table
		REPT	3,block
		REPT	8,char
		REPT	8,line
		db		LOW(SCREEN_BASE_PIXELS + 2048*block + 32*char + 256*line)
		ENDM
		ENDM
		ENDM

		ds		256-192

		REPT	3,block
		REPT	8,char
		REPT	8,line
		db		HIGH(SCREEN_BASE_PIXELS + 2048*block + 32*char + 256*line)
		ENDM
		ENDM
		ENDM

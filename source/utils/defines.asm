; ---------------------------------------------------------
; colour attribute defines
COLOUR_FG_BLACK			equ		000000b
COLOUR_FG_BLUE			equ		000001b
COLOUR_FG_RED			equ		000010b
COLOUR_FG_MAGENTA		equ		000011b
COLOUR_FG_GREEN			equ		000100b
COLOUR_FG_CYAN			equ		000101b
COLOUR_FG_YELLOW		equ		000110b
COLOUR_FG_WHITE			equ		000111b
COLOUR_BG_BLACK			equ		000000b
COLOUR_BG_BLUE			equ		001000b
COLOUR_BG_RED			equ		010000b
COLOUR_BG_MAGENTA		equ		011000b
COLOUR_BG_GREEN			equ		100000b
COLOUR_BG_CYAN			equ		101000b
COLOUR_BG_YELLOW		equ		110000b
COLOUR_BG_WHITE			equ		111000b
COLOUR_FLASH			equ		10000000b
COLOUR_BRIGHT			equ		01000000b

; rom addresses
CHARSET					equ		15360

; screen addresses
SCREEN_BASE_PIXELS		equ 	49152
SCREEN_SIZE_PIXELS		equ		2048 * 3
SCREEN_BASE_ATTRS		equ 	SCREEN_BASE_PIXELS + SCREEN_SIZE_PIXELS
SCREEN_SIZE_ATTRS		equ		32 * 24
SCREEN_SIZE				equ		SCREEN_SIZE_PIXELS + SCREEN_SIZE_ATTRS

; ports
AYCTRL_PORT				equ		0xfffd
AYDATA_PORT				equ		0xbffd
PAGING_PORT				equ		0x7ffd
BORDER_PORT				equ		254


; ---------------------------------------------------------
SLOW_MEM_START			equ		23808
SLOW_MEM_END			equ		32768-2048
SLOW_MEM_SIZE			equ		SLOW_MEM_END-SLOW_MEM_START

MODULE_START			equ		32768+1024


; ---------------------------------------------------------
SWAP_ON_VSYNC			equ		32768
PULL_RESOURCE_SCRIPT	equ		SWAP_ON_VSYNC+5
PULL_RESOURCE			equ		PULL_RESOURCE_SCRIPT+25



; ---------------------------------------------------------
RS_ITEM	MACRO ?resource_name, ?destination
	db		?resource_name, 0
	dw		?destination
ENDM
RS_END MACRO
	db		0
ENDM


; ---------------------------------------------------------
FIRST_OTHERS MACRO ?first, ?others
			ld		a,b
			or		a
			jp		nz,?others
			ld		a,c
			or		a
			jp		z,?first
			jp		?others
ENDM

FIRST_SECOND_OTHERS MACRO ?first, ?second, ?others
			ld		a,b
			or		a
			jp		nz,?others
			ld		a,c
			or		a
			jp		z,?first
			dec		a
			jp		z,?second
			jp		?others
ENDM

FIRST_SECOND_THIRD_OTHERS MACRO ?first, ?second, ?third, ?others
			ld		a,b
			or		a
			jp		nz,?others
			ld		a,c
			or		a
			jp		z,?first
			dec		a
			jp		z,?second
			dec		a
			jp		z,?third
			jp		?others
ENDM


; ---------------------------------------------------------
ALIGN_BY MACRO ?a
			REPT ?a - ($ & (?a-1))
			db		255
			ENDM
ENDM



; ---------------------------------------------------------
ALIGN_AT MACRO ?a
			REPT ?a - $
			db		255
			ENDM
ENDM

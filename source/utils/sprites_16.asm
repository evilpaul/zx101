; ---------------------------------------------------------
MAX_SPRITES	equ		14


frame_flag	db		0
frame_ptr	dw		frame_1



;			ALIGN_BY	64
frame_1		ds		64*MAX_SPRITES;1+(2+3*16)*MAX_SPRITES
frame_2		ds		64*MAX_SPRITES;1+(2+3*16)*MAX_SPRITES


; ---------------------------------------------------------
INLINE_GET_NEXT_ROW_DE MACRO
			inc		d
;			ld		a,d
;			and		7
;			jr		nz,$+12
			ld		a,e
			add		a,32
			ld		e,a
			jr		c,$+6
			ld		a,d
			sub		8
			ld		d,a
ENDM
INLINE_GET_NEXT_ROW_HL MACRO
			inc		h
;			ld		a,h
;			and		7
;			jr		nz,$+12
			ld		a,l
			add		a,32
			ld		l,a
			jr		c,$+6
			ld		a,h
			sub		8
			ld		h,a
ENDM


; ---------------------------------------------------------
DRAW_RIGHT MACRO
			ld		a,(de)	;r1
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
			inc		e
			ld		a,(de)	;r2
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
			inc		e
			ld		a,(de)	;r3
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
ENDM

DRAW_LEFT MACRO
			ld		a,(de)	;l1
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
			dec		e
			ld		a,(de)	;l2
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
			dec		e
			ld		a,(de)	;l3
			ld		(hl),a	; save under the sprite
			inc		l
			pop		bc		; read mask+data
			and		c
			or		b
			ld		(de),a
ENDM

ERASE_RIGHT	MACRO
			pop		bc
			ld		(hl),c
			inc		l
			ld		(hl),b
			inc		l
			pop		bc
			ld		(hl),c
ENDM
ERASE_LEFT MACRO
			ld		(hl),b
			dec		l
			pop		bc
			ld		(hl),c
			dec		l
			ld		(hl),b
ENDM



; ---------------------------------------------------------
erase_sprites_16
			di
			ld		(_sp_restore+1),sp		; we'll use the stack so save it first

			; swap frames
	ld	a,5
	out	(254),a
			ld		a,(frame_flag)
			xor		1
			ld		(frame_flag),a
			ld		hl,frame_2
			jr		nz,_odd
			ld		hl,frame_1
_odd		ld		(frame_ptr),hl



			; restore under sprites
			ld		hl,(frame_ptr)
			ld		a,(hl)
			inc		hl
			ld		b,a

;			add		a,a						; point hl to end of last sprite restore data
;			add		a,a
;			add		a,a
;			add		a,a
;			ld		d,0
;			ld		e,a
;			add		hl,de
;			add		hl,de
;			add		hl,de
;			ld		e,b
;			add		hl,de
;			add		hl,de
	add		a,a
	add		a,a
	add		a,a
	add		a,a
	ld		d,0
	ld		e,a
	add		hl,de
	add		hl,de
	add		hl,de
	add		hl,de

			ld		a,b						; a is our counter
			or		a
_restore_all
			jp		z,_done_restore
			ex		af,af'					; save counter

			ld		bc,-64;-1*(2+3*16)			; step backwards to start of record
			add		hl,bc

			ld		d,(hl)					; screen address to restore to
			inc		l
			ld		e,(hl)
			inc		l

			ld		(_sp1_set+1),hl
_sp1_set	ld		sp,1234

			dec	hl							; point hl back to start of this record
			dec	hl

			ex		de,hl
			ld		a,h
			rlca
			and		%00001110
			exx
			ld		hl,_sprite_erase_jt
			ld		d,0
			ld		e,a
			add		hl,de
			ld		a,(hl)
			inc		l
			ld		h,(hl)
			ld		l,a
			jp		(hl)
_sprite_erase_done
			ex		de,hl

			ex		af,af'					; restore counter..
			dec		a						; decrement..
			jp		_restore_all			; repeat..
_done_restore
_sp_restore	ld	sp,1234
			ei
			; --------

			ld		hl,(frame_ptr)
			ld		(hl),0						; reset count of drawn sprites


			ret



; ---------------------------------------------------------
			ALIGN_BY	2
_sprite_erase_jt
			dw			_sprite_erase_00
			dw			_sprite_erase_01
			dw			_sprite_erase_02
			dw			_sprite_erase_03
			dw			_sprite_erase_04
			dw			_sprite_erase_05
			dw			_sprite_erase_06
			dw			_sprite_erase_07

; ---------------------------------------------------------
_sprite_erase_00
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_01
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_02
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_03
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_04
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_05
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_06
			exx
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			INLINE_GET_NEXT_ROW_HL
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done

; ---------------------------------------------------------
_sprite_erase_07
			exx
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			INLINE_GET_NEXT_ROW_HL
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			inc		h
			ERASE_RIGHT
			inc		h
			ERASE_LEFT
			jp		_sprite_erase_done





; ---------------------------------------------------------
draw_sprites_16
			di
			ld		(_sp_restore+1),sp			; we'll use the stack so save it first


			ld		hl,(frame_ptr)
			push	hl
			pop		iy							; iy now points to count of drawn sprites
			inc		hl							; hl points to first sprite save buffer
			ld		(_save_ptr+1),hl

			inc		a
_all_sprites
			dec		a
			jp		z,_done
			ex		af,af'

	ld	a,1
	out	(254),a

			; sprite frame in hl
			ld		a,(ix+0)
			cp		256-16+1					; clip against right edge?
			jp		nc,_sprite_draw_clipped		; clipped
			and		%00000111
			add		a,a
			add		a,a
			add		a,a
			add		a,a
			add		a,a
			ld		h,(ix+3)
			ld		l,(ix+2)
			ld		d,0
			ld		e,a
			add		hl,de
			add		hl,de
			add		hl,de

			; screen address in de
			ld		a,(ix+1)
			cp		192-16+1					; clip against bottom edge?
			jp		nc,_sprite_draw_clipped		; clipped
			ld		b,HIGH(line_table)
			ld		c,a
			ld		a,(bc)
			ld		e,a
			inc		b
			ld		a,(bc)
			ld		d,a
			ld		a,(ix+0)
			rrca
			rrca
			rrca
			and		%000011111
			add		a,e
			ld		e,a

			ld		a,(iy)						; how many sprites drawn so far?
			cp		MAX_SPRITES
			jp		z,_done						; oh dear.. run out of sprite save slots
			inc		(iy)						; one more sprite drawn


			ld		(_sp2_set+1),hl
_sp2_set	ld		sp,1234
_save_ptr	ld		hl,1234
			ld		(hl),d
			inc		l
			ld		(hl),e
			inc		l

	ld	a,7
	out	(254),a

			ld		a,(ix+1)
			rlca
			and		%00001110
			exx
			ld		hl,_sprite_draw_jt
			ld		d,0
			ld		e,a
			add		hl,de
			ld		a,(hl)
			inc		l
			ld		h,(hl)
			ld		l,a
			jp		(hl)
_sprite_draw_done

		ld	bc,64-50
		add	hl,bc
			ld		(_save_ptr+1),hl
_sprite_draw_clipped
			ld		bc,4
			add		ix,bc
			ex		af,af'
			jp		_all_sprites

_done
_sp_restore	ld	sp,1234
			ei
			; --------



			ret


; ---------------------------------------------------------
			ALIGN_BY	2
_sprite_draw_jt
			dw			_sprite_draw_00
			dw			_sprite_draw_01
			dw			_sprite_draw_02
			dw			_sprite_draw_03
			dw			_sprite_draw_04
			dw			_sprite_draw_05
			dw			_sprite_draw_06
			dw			_sprite_draw_07


; ---------------------------------------------------------
_sprite_draw_00
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_01
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_02
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_03
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_04
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_05
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_06
			exx
			DRAW_RIGHT		; 0
			inc		d
			DRAW_LEFT		; 1
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			inc		d
			DRAW_LEFT		; 9
			INLINE_GET_NEXT_ROW_DE
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done

; ---------------------------------------------------------
_sprite_draw_07
			exx
			DRAW_RIGHT		; 0
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 1
			inc		d
			DRAW_RIGHT		; 2
			inc		d
			DRAW_LEFT		; 3
			inc		d
			DRAW_RIGHT		; 4
			inc		d
			DRAW_LEFT		; 5
			inc		d
			DRAW_RIGHT		; 6
			inc		d
			DRAW_LEFT		; 7
			inc		d
			DRAW_RIGHT		; 8
			INLINE_GET_NEXT_ROW_DE
			DRAW_LEFT		; 9
			inc		d
			DRAW_RIGHT		; 10
			inc		d
			DRAW_LEFT		; 11
			inc		d
			DRAW_RIGHT		; 12
			inc		d
			DRAW_LEFT		; 13
			inc		d
			DRAW_RIGHT		; 14
			inc		d
			DRAW_LEFT		; 15
			jp		_sprite_draw_done



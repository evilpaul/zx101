; ---------------------------------------------------------
get_next_row_de
			inc		d
			ld		a,d
			and		7
			ret		nz
			ld		a,e
			add		a,32
			ld		e,a
			ret		c
			ld		a,d
			sub		8
			ld		d,a
			ret


; ---------------------------------------------------------
get_next_row_hl
			inc		h
			ld		a,h
			and		7
			ret		nz
			ld		a,l
			add		a,32
			ld		l,a
			ret		c
			ld		a,h
			sub		8
			ld		h,a
			ret


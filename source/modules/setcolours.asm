include		'source\utils\defines.asm'
			org	MODULE_START


			FIRST_SECOND_OTHERS	set_colour, set_colour, done

set_colour	push	de
			ld		hl,SCREEN_BASE_PIXELS
			ld		(hl),d
			ld		de,SCREEN_BASE_PIXELS+1
			ld		bc,SCREEN_SIZE_PIXELS-1
			call	fast_ldir
			pop		de

			ld		hl,SCREEN_BASE_ATTRS
			ld		(hl),e
			ld		de,SCREEN_BASE_ATTRS+1
			ld		bc,SCREEN_SIZE_ATTRS-1
			call	fast_ldir

			call	SWAP_ON_VSYNC
done
			ret


include		'source\utils\fast_ldir.asm'

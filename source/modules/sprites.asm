include		'source\utils\defines.asm'
			org	MODULE_START


SPRITE_X_SPEED		equ	627
SPRITE_Y_SPEED		equ	493
SPRITE_X_START		equ	56437
SPRITE_Y_START		equ	12345
SPRITE_X_STEP		equ	7
SPRITE_Y_STEP		equ	5




start		FIRST_SECOND_OTHERS	first, second, others

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

second		ld		hl,bg_buffer
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE_PIXELS
			ldir

			call	SWAP_ON_VSYNC
			ret

others
			; move
			ld		ix,sprite_data

_p1y		ld		hl,SPRITE_Y_START
			ld		bc,SPRITE_Y_SPEED
			add		hl,bc
			ld		(_p1y+1),hl
			ld		e,h
			ld		d,HIGH(sin_table)

_p1x		ld		hl,SPRITE_X_START
			ld		bc,SPRITE_X_SPEED
			add		hl,bc
			ld		(_p1x+1),hl
			ld		l,h
			ld		h,HIGH(sin_table)

			ld		b,14
_spin1		ld		a,(hl)
			add		a,256/2-8
			ld		(ix+0),a
			ld		a,l
			add		a,SPRITE_X_STEP
			ld		l,a
			ld		a,(de)
			add		a,192/2-8
			ld		(ix+1),a
			ld		a,e
			add		a,SPRITE_Y_STEP
			ld		e,a
			inc		ix
			inc		ix
			inc		ix
			inc		ix
			djnz	_spin1

			call	erase_sprites_16
			ld		ix,sprite_data
			ld		a,14
			call	draw_sprites_16
			call	SWAP_ON_VSYNC
			ret




			include	'source\utils\line_table.asm'
			include	'source\utils\sprites_16.asm'





resource_script
			RS_ITEM	'sprite_bg',bg_buffer
			RS_ITEM	'sprite_1',sprite_1
			RS_ITEM	'sprite_2',sprite_2
			RS_ITEM	'sprite_3',sprite_3
			RS_ITEM	'sprite_4',sprite_4
			RS_ITEM	'sprites_sin',sin_table
			RS_END



bg_buffer	equ		SLOW_MEM_START

ALIGN_BY	256
sin_table	ds		256

sprite_data
			db		10,35	; 1
			dw		sprite_4
			db		40,40	; 2
			dw		sprite_4
			db		120,120	; 3
			dw		sprite_4
			db		50,170	; 4
			dw		sprite_3
			db		60,170	; 5
			dw		sprite_3
			db		7,170	; 6
			dw		sprite_3
			db		80,170	; 7
			dw		sprite_3
			db		90,170	; 8
			dw		sprite_2
			db		100,170	; 9
			dw		sprite_2
			db		110,170	; 10
			dw		sprite_2
			db		120,170	; 11
			dw		sprite_2
			db		130,170	; 12
			dw		sprite_1
			db		140,170	; 13
			dw		sprite_1
			db		150,170	; 14
			dw		sprite_1



sprite_1	ds		3*16*2*8
sprite_2	ds		3*16*2*8
sprite_3	ds		3*16*2*8
sprite_4	ds		3*16*2*8


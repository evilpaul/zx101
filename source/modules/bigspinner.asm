include		'source\utils\defines.asm'
			org	MODULE_START


COPY_ROW 	MACRO	?dest
			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest+16
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af

			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest+32
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
ENDM




start		FIRST_SECOND_THIRD_OTHERS	first, second, third, others

first		ld		hl,resource_script_1
			call	PULL_RESOURCE_SCRIPT
			
second		ld		hl,SLOW_MEM_START
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE
			ldir
			ld		hl,SCREEN_BASE_ATTRS+32*6
			ld		de,SCREEN_BASE_ATTRS+32*6+1
			ld		bc,32*12-1
			ld		(hl),COLOUR_FG_WHITE|COLOUR_BG_BLUE|COLOUR_BRIGHT
			ldir
			call	SWAP_ON_VSYNC
			ret

third		ld		hl,resource_script_2
			call	PULL_RESOURCE_SCRIPT
			ret

others



			; animate
_pos		ld		a,0
			add		a,2
_mod		cp		5*2
			jr		nz,_ok
			xor		a
_ok			ld		(_pos+1),a

			; locate source frame
			ld		d,0
			ld		e,a
			ld		hl,src_table
			add		hl,de
			ld		e,(hl)
			inc		hl
			ld		d,(hl)

			; set-up copy
			ld		a,6
			out		(254),a
			ld		hl,_draw + 6
_do_setup
			; test...
			ld		b,0
			ld		a,96;192/2
_set		ex		af,af'

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		c,25
			add		hl,bc
			ex		de,hl
			ld		c,16
			add		hl,bc
			ex		de,hl

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		c,25
			add		hl,bc
			ex		de,hl
			ld		c,16
			add		hl,bc
			ex		de,hl

			ex		af,af'
			dec		a
			jr		nz,_set

			ld		a,5
			out		(254),a

_draw
			di
			ld		(_sp_restore+1),sp

			REPT 2,char,6
				REPT 8,line
					COPY_ROW	SCREEN_BASE_PIXELS + 256*line + 32*char + 2048*0
				ENDM
			ENDM
			REPT 8,char
				REPT 8,line
					COPY_ROW	SCREEN_BASE_PIXELS + 256*line + 32*char + 2048*1
				ENDM
			ENDM
			REPT 2,char
				REPT 8,line
					COPY_ROW	SCREEN_BASE_PIXELS + 256*line + 32*char + 2048*2
				ENDM
			ENDM

_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret


resource_script_1
			RS_ITEM	'twister_bg',SLOW_MEM_START
			RS_END

resource_script_2
			RS_ITEM	'twister1',data_src1
			RS_ITEM	'twister2',data_src2
			RS_ITEM	'twister3',data_src3
			RS_ITEM	'twister4',data_src4
			RS_ITEM	'twister5',data_src5
			RS_END


data_src1	ds		96*32
data_src2	ds		96*32
data_src3	ds		96*32
data_src4	equ		SLOW_MEM_START
data_src5	equ		data_src4 + 96*32

src_table	dw		data_src1
			dw		data_src2
			dw		data_src3
			dw		data_src4
			dw		data_src5


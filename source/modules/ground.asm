include		'source\utils\defines.asm'
			org	MODULE_START


COPY_ROW 	MACRO	?dest
			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		de
			pop		hl
			pop		ix

			ld		sp,?dest+16
			push	ix
			push	hl
			push	de
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af

			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		de
			pop		hl
			pop		ix

			ld		sp,?dest+32
			push	ix
			push	hl
			push	de
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
ENDM


start		FIRST_SECOND_OTHERS	first, second, others

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

second		ld		hl,SCREEN_BASE_PIXELS
			ld		de,SCREEN_BASE_PIXELS+1
			ld		(hl),0
			ld		bc,SCREEN_SIZE_PIXELS-1
			ldir

			ld		hl,SCREEN_BASE_ATTRS+256*0
			ld		de,SCREEN_BASE_ATTRS+256*0+1
			ld		(hl),COLOUR_FG_WHITE|COLOUR_BG_BLACK
			ld		bc,512-1
			ldir
			ld		hl,SCREEN_BASE_ATTRS+256*2
			ld		de,SCREEN_BASE_ATTRS+256*2+1
			ld		(hl),COLOUR_FG_YELLOW|COLOUR_BG_BLUE|COLOUR_BRIGHT
			ld		bc,256-1
			ldir

			call	SWAP_ON_VSYNC
			ret


;0		00100000 00000000
;2047	‭00100111 11111111‬
;2048	0010‭1000 00000000‬
;4095	0010‭1111 11111111‬

others		ld		a,6
			out		(254),a

			; data address
			ld		de,data_ground_1

			; point to first source address
			ld		hl,_draw_rout+1

			exx
_pos		ld		hl,0
			ld		de,-350		; move speed
			add		hl,de
			ld		(_pos+1),hl
			ld		de,600		; initial step
			ld		bc,-8		; step reduction
			exx

			ld		b,0
			ld		a,64
_set		ex		af,af'
			ld		a,d
			and		%11010111
			exx
			add		hl,de
			ex		de,hl
			add		hl,bc
			ex		de,hl
			bit		2,h
			exx
			jr		nz,_g2
_g1			or		%00101000
_g2			or		%00100000
			ld		d,a

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		c,25
			add		hl,bc
			set		4,e

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		c,25
			add		hl,bc
			ex		de,hl
			ld		c,16
			add		hl,bc
			ex		de,hl

			ex		af,af'
			dec		a
			jr		nz,_set


			ld		a,5
			out		(254),a

			di
			ld		(_sp_restore+1),sp

_draw_rout
			REPT 8,char
			REPT 8,line
			COPY_ROW	SCREEN_BASE_PIXELS + 2048*2 + 256*line + 32*char
			ENDM
			ENDM

_sp_restore	ld		sp,1234
			ei


SCROLLER_ROW 	MACRO	?source_line, ?dest
		ld		hl,scroll_buffer + 32*?source_line + 31
		ld		de,?dest+31

		ld		a,(scroll_next_char+?source_line)
		rla
		ld		(scroll_next_char+?source_line),a

		REPT	31
		ld		a,(hl)
		rla
		ld		(hl),a
		dec		l
		ld		(de),a
		dec		e
		ENDM
		ld		a,(hl)
		rla
		ld		(hl),a
		ld		(de),a
ENDM


;--
		ld		a,4
		out		(254),a

		SCROLLER_ROW	0, SCREEN_BASE_PIXELS + 32*4 + 256*0
		SCROLLER_ROW	1, SCREEN_BASE_PIXELS + 32*4 + 256*1
		SCROLLER_ROW	2, SCREEN_BASE_PIXELS + 32*4 + 256*2
		SCROLLER_ROW	3, SCREEN_BASE_PIXELS + 32*4 + 256*3
		SCROLLER_ROW	4, SCREEN_BASE_PIXELS + 32*4 + 256*4
		SCROLLER_ROW	5, SCREEN_BASE_PIXELS + 32*4 + 256*5
		SCROLLER_ROW	6, SCREEN_BASE_PIXELS + 32*4 + 256*6
		SCROLLER_ROW	7, SCREEN_BASE_PIXELS + 32*4 + 256*7
		SCROLLER_ROW	8, SCREEN_BASE_PIXELS + 32*5 + 256*0
		SCROLLER_ROW	9, SCREEN_BASE_PIXELS + 32*5 + 256*1
		SCROLLER_ROW	10, SCREEN_BASE_PIXELS + 32*5 + 256*2
		SCROLLER_ROW	11, SCREEN_BASE_PIXELS + 32*5 + 256*3
		SCROLLER_ROW	12, SCREEN_BASE_PIXELS + 32*5 + 256*4
		SCROLLER_ROW	13, SCREEN_BASE_PIXELS + 32*5 + 256*5
		SCROLLER_ROW	14, SCREEN_BASE_PIXELS + 32*5 + 256*6
		SCROLLER_ROW	15, SCREEN_BASE_PIXELS + 32*5 + 256*7


_step	ld		a,0
		inc		a
		and		%111
		ld		(_step+1),a
		jr		nz,_no_new_char

		ld		hl,(scroller_text_ptr)
		ld		a,(hl)
		or		a
		jr		nz,_not_wrapped
		ld		hl,scroller_text
		ld		a,(hl)
_not_wrapped
		inc		hl
		ld		(scroller_text_ptr),hl

		ld		h,0
		ld		l,a
		add		hl,hl
		add		hl,hl
		add		hl,hl
		add		hl,hl
		ld		de,font_data
		add		hl,de

		ex		de,hl
		ld		hl,scroll_next_char
		REPT	16
		ld		a,(de)
		inc		e
		ld		(hl),a
		inc		hl
		ENDM
_no_new_char
;--

			call	SWAP_ON_VSYNC
			ret



resource_script
			RS_ITEM	'ground_1',data_ground_1
			RS_ITEM	'ground_2',data_ground_2
			RS_ITEM	'8_by_16_font',font_data
			RS_END

scroller_text
			db		'*** 101 - AteBit Demo System ***    *** Greets to Elites ***    ',0
scroller_text_ptr
			dw		scroller_text
scroll_next_char
			ds		16
font_data	equ		SLOW_MEM_START


			ALIGN_BY	8192
data_ground_1	ds		2048
data_ground_2	ds		2048
scroll_buffer	ds		32*16

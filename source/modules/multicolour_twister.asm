include		'source\utils\defines.asm'
			org	MODULE_START


			jp		start
			jp		vbl_top


start		FIRST_SECOND_OTHERS	first, second, others

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

second		ld		hl,background_image
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE
			ldir
			call	SWAP_ON_VSYNC

others		ret

resource_script
			RS_ITEM	'rgb_twister',twister_gfx
			RS_ITEM 'rgb_twister_sin',sin_table
			RS_ITEM	'multicolour_bg',background_image
			RS_END



base		dw		0
speed		dw		256
dly			db		33

;882
;228*4=912
; 228 per scanline

COPY_LINE	MACRO	?dest
			ld		hl,(base)
			ld		bc,(speed)
			add		hl,bc
			ld		(base),hl
			ld		a,h
			add		a,a
			ld		d,0
			ld		e,a
			ld		hl,0
			add		hl,de
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,de
			add		hl,de
			ld		de,twister_gfx
			add		hl,de
			ld		sp,hl



			ld	a,(dly)									; 7
			ld	b,a
			djnz	$									; 13/8

	ld	a,2											; 7
	out	(254),a										; 11
			pop		af									; 10
			pop		bc									; 10
			pop		de									; 10
			pop		hl									; 10
			exx											; 4
			pop		bc									; 10
			pop		de									; 10
			pop		hl									; 10
			pop		ix									; 14
			pop		iy									; 14
			ld		sp,?dest + 18						; 10			106
			push	iy									; 15
			push	ix									; 15
			push	hl									; 11
			push	de									; 11
			push	bc									; 11
			exx											; 4
			push	hl									; 11
			push	de									; 11
			push	bc									; 11
			push	af									; 11
	ld	a,5											; 7
	out	(254),a										; 11
ENDM

vbl_top
			nop
			nop
			nop
			nop
			nop
			nop
			ld		a,1
			out		(254),a
			ld		bc,504
_w			dec		bc
			ld		a,b
			or		c
			jr		nz,_w

			ld		(_sp_restore+1),sp
			REPT	24,line
			COPY_LINE	SCREEN_BASE_ATTRS + line*32 + 7
			COPY_LINE	SCREEN_BASE_ATTRS + line*32 + 7
			ENDM
_sp_restore	ld		sp,1234


_s1	ld	hl,0
	ld	de,157
	add	hl,de
	ld	(_s1+1),hl
	ld	l,h
	ld	h,HIGH(sin_table)
	ld	l,(hl)
	ld	h,0
	add	hl,hl
	add	hl,hl
	add	hl,hl
	ld	de,31868
	add	hl,de
	ex	de,hl

_b	ld	hl,0
	add	hl,de
	ld	(_b+1),hl
	ld	(base),hl

_s	ld	hl,0
	ld	de,81
	add	hl,de
	ld	(_s+1),hl
	ld	l,h
	ld	h,HIGH(sin_table)
	ld	l,(hl)
	ld	h,0
	add	hl,hl
	add	hl,hl
	add	hl,hl
	ld	(speed),hl

			ret



			ALIGN_BY	256
sin_table			ds	256
background_image	equ	SLOW_MEM_START
twister_gfx			ds	18*256

include		'source\utils\defines.asm'
			org	MODULE_START



COPY_ROW 	MACRO	?dest1,?dest2
			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest1+16
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
			exx
			ld		sp,?dest2+16
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af


			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest1+32
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
			exx
			ld		sp,?dest2+32
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
ENDM


start
			FIRST_OTHERS	first, others

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT
			ret

others

			; speed
_spd		ld		hl,0
			ld		de,50
			add		hl,de
			ld		(_spd+1),hl
			ld		l,h
			ld		h,HIGH(sin_table)
			ld		a,(hl)
			sra		a
			sra		a
			ld		b,a

			; choose frame
_rot		ld		a,0
			add		a,b
			ld		(_rot+1),a
			rr		a
			rr		a
			rr		a
			rr		a
			and		%1110

			; locate source data
			ld		hl,data_src_table
			ld		d,0
			ld		e,a
			add		hl,de
			ld		e,(hl)
			inc		hl
			ld		d,(hl)

			; set-up copy
			ld		a,c
			and		%1
			ld		hl,draw_even
			jp		z,_do_setup
			ld		hl,draw_odd
_do_setup
			; set the run address
			ld		a,l
			ld		(_draw+1),a
			ld		a,h
			ld		(_draw+2),a

			; point to first source address
			ld		bc,6
			add		hl,bc
			ld		a,6
			out		(254),a

			; test...
			ld		a,192/2/2
_set		ex		af,af'

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		bc,25+14
			add		hl,bc
			ex		de,hl
			ld		bc,16
			add		hl,bc
			ex		de,hl

			ld		(hl),e
			inc		hl
			ld		(hl),d
			ld		bc,25+14
			add		hl,bc
			ex		de,hl
			ld		bc,16
			add		hl,bc
			ex		de,hl

			ex		af,af'
			dec		a
			jr		nz,_set

			ld		a,5
			out		(254),a
_draw		jp		1234		



draw_even
			di
			ld		(_sp_restore+1),sp

			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*0 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*7 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*1 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*6 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*2 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*5 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*3 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*4 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*4 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*3 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*5 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*2 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*6 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*1 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*7 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*0 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*0 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*7 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*1 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*6 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*2 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*5 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*3 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2 + 32*4 + 2048*1
			ENDM

_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret
			ret

draw_odd
			di
			ld		(_sp_restore+1),sp

			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*0 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*7 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*1 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*6 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*2 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*5 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*3 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*4 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*4 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*3 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*5 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*2 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*6 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*1 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*7 + 2048*0, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*0 + 2048*2
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*0 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*7 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*1 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*6 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*2 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*5 + 2048*1
			ENDM
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2+256 + 32*3 + 2048*1, SCREEN_BASE_PIXELS + 256*(3-line)*2+256 + 32*4 + 2048*1
			ENDM

_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret


resource_script
			RS_ITEM	'sin_table',sin_table
			RS_ITEM	'tunnel1',data_src1
			RS_ITEM	'tunnel2',data_src2
			RS_ITEM	'tunnel3',data_src3
			RS_ITEM	'tunnel4',data_src4
			RS_ITEM	'tunnel5',data_src5
			RS_ITEM	'tunnel6',data_src6
			RS_ITEM	'tunnel7',data_src7
			RS_ITEM	'tunnel8',data_src8
			RS_END

data_src_table
			dw		data_src1
			dw		data_src2
			dw		data_src3
			dw		data_src4
			dw		data_src5
			dw		data_src6
			dw		data_src7
			dw		data_src8

data_src1	ds		1024*3/2
data_src2	ds		1024*3/2
data_src3	ds		1024*3/2
data_src4	ds		1024*3/2
data_src5	equ		SLOW_MEM_START
data_src6	equ		data_src5+3*1024/2
data_src7	equ		data_src6+3*1024/2
data_src8	equ		data_src7+3*1024/2

ALIGN_BY	256
sin_table	ds		256


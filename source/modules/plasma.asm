include		'source\utils\defines.asm'
			org	MODULE_START


start		FIRST_SECOND_OTHERS	first, second, others

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

second		ld		hl,background_image
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE_PIXELS
			ldir

			call	SWAP_ON_VSYNC
			ret


others

_x1			ld		hl,0
			ld		bc,233
			add		hl,bc
			ld		(_x1+1),hl
			ld		b,HIGH(data_sin)
			ld		c,h
			ld		a,(bc)
			rrca
			rrca
			and		%00011111
			ld		(_or1+1),a
_y1			ld		hl,0
			ld		bc,155
			add		hl,bc
			ld		(_y1+1),hl
			ld		b,HIGH(data_sin)
			ld		c,h
			ld		a,(bc)
			and		%01111100
			ld		h,0
			ld		l,a
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			ld		a,l
_or1		or		123
			ld		l,a
			ex		de,hl

_x2			ld		hl,1432
			ld		bc,53
			add		hl,bc
			ld		(_x2+1),hl
			ld		b,HIGH(data_sin)
			ld		c,h
			ld		a,(bc)
			rrca
			rrca
			and		%00011111
			ld		(_or2+1),a
_y2			ld		hl,5145
			ld		bc,-52
			add		hl,bc
			ld		(_y2+1),hl
			ld		b,HIGH(data_sin)
			ld		c,h
			ld		a,(bc)
			and		%01111100
			ld		h,0
			ld		l,a
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			ld		a,l
_or2		or		123
			ld		l,a


			exx
			ld		h,HIGH(data_colour_ramp)
			ld		de,SCREEN_BASE_ATTRS
			exx

			REPT	24,row
			ld		a,l
			and		%00011111
			ld		b,a
			ld		a,l
			and		%11000000
			or		b
			ld		l,a
			ld		bc,%01000000
			add		hl,bc
			ld		a,h
			and		%00000111
			or		HIGH(data_plasma_1)
			ld		h,a

			ex		de,hl
			ld		a,l
			and		%00011111
			ld		b,a
			ld		a,l
			and		%11000000
			or		b
			ld		l,a
			ld		bc,%01000000
			add		hl,bc
			ld		a,h
			and		%00000111
			or		HIGH(data_plasma_2)
			ld		h,a
			ex		de,hl

			REPT	32,char
			inc		e
			ld		a,(de)
			inc		l
			add		a,(hl)

			exx
			ld		l,a
			ld		a,(hl)
			ld		(de),a
			IF char < 31
			inc		e
			ELSE
			inc		de
			ENDIF
			exx
			ENDM

			ENDM

			call	SWAP_ON_VSYNC
			ret


resource_script
			RS_ITEM	'sin_table',data_sin
			RS_ITEM	'plasma_1',data_plasma_1
			RS_ITEM	'plasma_2',data_plasma_2
			RS_ITEM	'plasma_bg',background_image
			RS_END

			ALIGN_BY	256
data_sin			ds		256
data_plasma_1		ds		2048
data_plasma_2		ds		2048
background_image	equ	SLOW_MEM_START

data_colour_ramp
	REPT 64
			db		COLOUR_FG_BLUE
	ENDM
	REPT 40
			db		COLOUR_FG_BLUE|COLOUR_BRIGHT
	ENDM
	REPT 38
			db		COLOUR_FG_CYAN
	ENDM
	REPT 20
			db		COLOUR_FG_CYAN|COLOUR_BRIGHT
	ENDM
	REPT 94
			db		COLOUR_FG_WHITE|COLOUR_BRIGHT
	ENDM


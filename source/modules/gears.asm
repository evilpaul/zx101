include		'source\utils\defines.asm'
			org	MODULE_START


COPY_ROW 	MACRO	?pair1, ?pair2, ?dest
			ld		sp,?dest
			REPT	32/4
			push	?pair1
			push	?pair2
			ENDM
ENDM
COPY_ROWS	MACRO	?pair1, ?pair2, ?dest
			COPY_ROW	?pair1, ?pair2, ?dest - 32*4
			COPY_ROW	?pair1, ?pair2, ?dest - 32*0
ENDM
COPY_BLOCK	MACRO	?source, ?dest
			ld		sp,?source
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		ix
			pop		bc
			pop		de
			pop		hl
			COPY_ROWS	bc, ix, ?dest-256*2+2048*0
			COPY_ROWS	bc, ix, ?dest-256*2+2048*1
			COPY_ROWS	bc, ix, ?dest-256*2+2048*2
			COPY_ROWS	hl, de, ?dest-256*3+2048*0
			COPY_ROWS	hl, de, ?dest-256*3+2048*1
			COPY_ROWS	hl, de, ?dest-256*3+2048*2
			exx
			COPY_ROWS	bc, af, ?dest-256*0+2048*0
			COPY_ROWS	bc, af, ?dest-256*0+2048*1
			COPY_ROWS	bc, af, ?dest-256*0+2048*2
			COPY_ROWS	hl, de, ?dest-256*1+2048*0
			COPY_ROWS	hl, de, ?dest-256*1+2048*1
			COPY_ROWS	hl, de, ?dest-256*1+2048*2
ENDM

start		FIRST_OTHERS	first, not_first

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT
			ret

not_first	ld		a,6
			out		(254),a

_rot		ld		a,0
			add		a,2
			ld		(_rot+1),a
			ld		d,HIGH(sin_table)
			ld		e,a
			ld		a,(de)
			and		%0111110
			ld		h,0
			ld		l,a
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			add		hl,hl
			ld		de,data_src
			add		hl,de

_pos		ld		a,0
			add		a,-3
			ld		(_pos+1),a
			ld		d,HIGH(sin_table)
			ld		e,a
			ld		a,(de)
			and		%01111100
			add		a,l
			ld		l,a

			ld		de,data
			REPT	32
			ld		a,l
			add		a,4
			and		%01111111
			ld		b,a
			ld		a,l
			and		%10000000
			add		a,b
			ld		l,a
			push	hl
			ldi
			ldi
			ldi
			ldi
			pop		hl
			ENDM

		ld	a,5
		out(254),a


			di
			ld		(_sp_restore+1),sp

			COPY_BLOCK	data+4*0,  SCREEN_BASE_PIXELS+2048-1024*0-32*0
			COPY_BLOCK	data+4*4,  SCREEN_BASE_PIXELS+2048-1024*1-32*0
			COPY_BLOCK	data+4*8,  SCREEN_BASE_PIXELS+2048-1024*0-32*1
			COPY_BLOCK	data+4*12, SCREEN_BASE_PIXELS+2048-1024*1-32*1
			COPY_BLOCK	data+4*16, SCREEN_BASE_PIXELS+2048-1024*0-32*2
			COPY_BLOCK	data+4*20, SCREEN_BASE_PIXELS+2048-1024*1-32*2
			COPY_BLOCK	data+4*24, SCREEN_BASE_PIXELS+2048-1024*0-32*3
			COPY_BLOCK	data+4*28, SCREEN_BASE_PIXELS+2048-1024*1-32*3


_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret




data		REPT	32
			db		0,0,0,0
			ENDM

resource_script
			RS_ITEM	'sin_table',sin_table
			RS_ITEM	'gear_anim',data_src
			RS_END


ALIGN_BY	256
sin_table
			REPT	256
			db	0
			ENDM
data_src

include		'source\utils\defines.asm'
			org	MODULE_START


			jp		start
			jp		vbl_top


start		FIRST_SECOND_OTHERS	first, second, others


first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

second		ld		hl,background_image
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE
			ldir
			call	SWAP_ON_VSYNC

others		ret

resource_script
			RS_ITEM	'multicolour_pic',display_buffer
			RS_ITEM	'multicolour_bg',background_image
			RS_END



dly			db		48

;882
;228*4=912
; 228 per scanline

COPY_LINE	MACRO	?source,?dest
			ld	a,(dly)									; 7
			ld	b,a
			djnz	$									; 13/8

	ld	a,2											; 7
	out	(254),a										; 11
			ld		sp,?source							; 10			98
			pop		af									; 10
			pop		bc									; 10
			pop		de									; 10
			pop		hl									; 10
			exx											; 4
			pop		bc									; 10
			pop		de									; 10
			pop		hl									; 10
			pop		ix									; 14
			pop		iy									; 14
			ld		sp,?dest + 18						; 10			106
			push	iy									; 15
			push	ix									; 15
			push	hl									; 11
			push	de									; 11
			push	bc									; 11
			exx											; 4
			push	hl									; 11
			push	de									; 11
			push	bc									; 11
			push	af									; 11
	ld	a,5											; 7
	out	(254),a										; 11
ENDM

vbl_top
			nop
			nop
			nop
			nop
			nop
			nop
			ld		a,1
			out		(254),a
			ld		bc,504
_w			dec		bc
			ld		a,b
			or		c
			jr		nz,_w

			ld		(_sp_restore+1),sp
			REPT	24,line
			COPY_LINE	display_buffer + 18*(line*2+0), SCREEN_BASE_ATTRS + line*32 + 7
			COPY_LINE	display_buffer + 18*(line*2+1), SCREEN_BASE_ATTRS + line*32 + 7
			ENDM
_sp_restore	ld		sp,1234
			ret


display_buffer		ds	18*2*24
background_image	equ	SLOW_MEM_START


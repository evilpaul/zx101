include		'source\utils\defines.asm'
			org	MODULE_START


COPY_ROW 	MACRO	?dest
			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest+16
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af


			; 16 bytes
			ld		sp,1234
			pop		af
			pop		bc
			pop		de
			pop		hl
			exx
			pop		bc
			pop		hl
			pop		de
			pop		ix

			ld		sp,?dest+32
			push	ix
			push	de
			push	hl
			push	bc
			exx
			push	hl
			push	de
			push	bc
			push	af
ENDM


start		FIRST_OTHERS	first, not_first

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT
			ret

not_first

			; set-up copy
			ld		a,c
			and		%10
			ld		hl,draw_even
			jp		z,_do_setup
			ld		hl,draw_odd
_do_setup
			; set the run address
			ld		a,l
			ld		(_draw+1),a
			ld		a,h
			ld		(_draw+2),a

			; point to first source address
			ld		bc,6
			add		hl,bc
			ld		a,6
			out		(254),a

			; animate start position (store in hl)
			exx
_sin_start	ld		hl,0
			ld		bc,334
			add		hl,bc
			ld		(_sin_start+1),hl
			ld		l,h
			ld		h,HIGH(sin_table)
			ld		a,(hl)
			add		a,128
			ld		b,0
			ld		c,a
_start		ld		hl,0
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			add		hl,bc
			ld		(_start+1),hl

			; animate twist amount (store in bc)
			push	hl
_sin_spd	ld		hl,0
			ld		bc,249
			add		hl,bc
			ld		(_sin_spd+1),hl
			ld		l,h
			ld		h,HIGH(sin_table)
			ld		a,(hl)
			ld		h,0
	bit		7,a
	jr		z,_o1
	dec		h
_o1			ld		l,a
			add		hl,hl
			ld		b,h
			ld		c,l
			pop		hl

			; prepare for writing to copy routine
			ld		d,%11111110				; mask for 7 bits of position data
			exx
			ld		bc,25					; stride of copy routines

		REPT	192/2
			exx
			add		hl,bc					; advance position
			ld		a,h
			and		d						; get top 7 bits
			exx

			ex		de,hl					; get position data into 16 bits in de
			ld		h,HIGH(data_src)/16
			ld		l,a
			add		hl,hl					; * 4 (was already *2)
			add		hl,hl					; * 8
			add		hl,hl					; * 16
			add		hl,hl					; * 32
			ex		de,hl

			ld		(hl),e					; store address for first half
			inc		hl
			ld		(hl),d
			set		4,e						; add screen stride (the same as adding 16)
			add		hl,bc					; add copy routine stride
			ld		(hl),e					; store address for second half
			inc		hl
			ld		(hl),d
			add		hl,bc					; add copy routine stride
		ENDM

			ld		a,5
			out		(254),a
_draw		jp		1234		



draw_even
			di
			ld		(_sp_restore+1),sp

			REPT 3,block
			REPT 8,char
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*char + 2048*block
			ENDM
			ENDM
			ENDM

_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret

draw_odd
			di
			ld		(_sp_restore+1),sp

			REPT 3,block
			REPT 8,char
			REPT 4,line
			COPY_ROW	SCREEN_BASE_PIXELS + 256*line*2 + 32*char + 2048*block + 256
			ENDM
			ENDM
			ENDM

_sp_restore	ld		sp,1234
			ei
			call	SWAP_ON_VSYNC
			ret



resource_script
			RS_ITEM	'sin_table',sin_table
			RS_ITEM	'twister',data_src
			RS_END


ALIGN_BY	256
sin_table	ds		256

;data_src	equ		SLOW_MEM_START
data_src	equ		16*1024+8*1024


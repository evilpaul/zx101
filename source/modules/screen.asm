include		'source\utils\defines.asm'
			org	MODULE_START


start		FIRST_OTHERS	first, not_first

first		ld		hl,resource_script
			call	PULL_RESOURCE_SCRIPT

			ld		hl,gfx
			ld		de,SCREEN_BASE_PIXELS
			ld		bc,SCREEN_SIZE
			ldir

			call	SWAP_ON_VSYNC
			ret

not_first
			ret


resource_script
			RS_ITEM	'screen',gfx
			RS_END


gfx			ds		SCREEN_SIZE

			org		32768-2048

start		
			di
			ld		a,(0x5b5c)
			and		0xf8
			or		BANK_NUMBER
			ld		bc,0x7ffd
			out		(c),a

			ld		hl,data_start
			ld		de,49152
			ld		bc,data_end-data_start
			ld		a,b
			or		c
			jr		z,_empty_bank
			ldir
_empty_bank

			ld		a,(0x5b5c)
			ld		bc,0x7ffd
			out		(c),a
			ei

			ret

data_start	incbin 'build\data\cutter.temp.bin'
data_end

end start
